import EventEmitter from 'wolfy87-eventemitter';

import { getConfig } from './config';

const EVENT_NAME = 'RADIKS_STREAM_MESSAGE';

export default class Streamer {
  static initialized: boolean;
  static socket: WebSocket;
  static emitter: EventEmitter;

  static init() {
    if (this.initialized) {
      return;
    }
    this.emitter = new EventEmitter();
    this.initSocket();
    this.initialized = true;
  }

  static initSocket() {
    const { apiServer } = getConfig();
    const protocol = document.location.protocol === 'http:' ? 'ws' : 'wss';
    const socket = new WebSocket(`${protocol}://${apiServer.replace(/^https?:\/\//, '')}/radiks/stream/`);
    this.socket = socket;
    socket.onmessage = (event) => {
      this.emitter.emit(EVENT_NAME, [event]);
    };
    socket.onclose = (event) => {
      this.initialized = false;
      window.setTimeout(() => {
        this.initSocket();
      }, 5000);
    };
  }

  static addListener(callback: (args: any[]) => void) {
    this.init();
    this.emitter.addListener(EVENT_NAME, callback);
  }

  static removeListener(callback: Function) {
    this.init();
    this.emitter.removeListener(EVENT_NAME, callback);
  }
}
